//@ts-nocheck
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function main() {
  const beatrice = await prisma.user.create({
    data: {
      firstName: "Beatrice",
      lastName: "Feldman",
      anonymous: false,
      profilePic: "/profile2.png",

      requests: {
        create: {
          title: "Coast-to-coast transport",
          summary:
            "We need a reliable transportation system from one coast to the other. Every time I need to visit family on the other side of the island it takes FOREVER. We’re in 2023. We should have this by now.",
          priority: "high",
          numVotes: 0,
          categories: ["Transportation"],
        },
      },
    },
  });

  const fernando = await prisma.user.create({
    data: {
      firstName: "Fernando",
      lastName: "Hernandez",
      anonymous: false,
      profilePic: "/profile1.png",

      requests: {
        create: {
          title: "Bike lane on 4th st.",
          summary:
            "Now that traffic has really picked up, we could really use a bike lane on 4th st. It’s starting to get dangerous",
          priority: "medium",
          numVotes: 0,
          categories: ["Transportation"],
        },
      },
    },
  });

  const raphael = await prisma.user.create({
    data: {
      firstName: "Raphael",
      lastName: "Arar",
      anonymous: true,
      profilePic: "/profile3.png",

      requests: {
        create: {
          title: "Better care of our recreation facilities",
          summary:
            "The fields are pretty run down where my kids like to play soccer. They keep complaining about the amount of",
          priority: "low",
          numVotes: 0,
          categories: ["Transportation"],
        },
      },
    },
  });

  const sukie = await prisma.user.create({
    data: {
      firstName: "Sukie",
      lastName: "Kumazawa",
      anonymous: false,
      profilePic: "/profile3.png",
    },
  });

  console.log({ beatrice, fernando, raphael, sukie });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
