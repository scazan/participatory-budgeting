import { ChooseUserCard } from "../components/ChooseUserCard";

const Home = async () => {
  return (
    <main className="flex min-h-screen flex-col">
      <ChooseUserCard />
    </main>
  );
};

export default Home;
