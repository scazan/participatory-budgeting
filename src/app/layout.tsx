import "./globals.css";
import type { Metadata } from "next";
import { Roboto } from "next/font/google";
import { SiteHeader } from "@/components";
import { cn } from "@/lib/utils";

const robotoFont = Roboto({ subsets: ["latin"], weight: ["400", "500"] });

export const metadata: Metadata = {
  title: "Participatory Budgeting",
  description: "A participatory budgeting app",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={cn(robotoFont.className)}>{children}</body>
    </html>
  );
}
