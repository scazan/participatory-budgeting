import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import { getAllRequests } from "@/api";
import { SiteHeader } from "@/components";
import { Requests } from "@/components/Requests";
import { CreateRequestButton } from "@/components/Requests/CreateRequestButton";

export default async () => {
  const requests = await getAllRequests();

  return (
    <main className="flex min-h-screen flex-col pb-12">
      <SiteHeader className="p-6 px-4" />
      <Requests requests={requests} />
      <CreateRequestButton />
    </main>
  );
};
