import { getExpertOpinion } from "@/api/llm";

export async function POST(req: Request): Promise<Response> {
  let { requestText } = await req.json();

  const expertOpinion = await getExpertOpinion(requestText);

  return new Response(expertOpinion, {
    status: 200,
  });
}
