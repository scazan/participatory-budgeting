import { getRequest } from "@/api";
import { RequestDetails } from "@/components/Requests/RequestDetails";

const RequestPage = async (props) => {
  const {
    params: { id },
  } = props;

  const request = await getRequest({ id: parseInt(id, 10) });

  return (
    <main className="flex min-h-screen flex-col py-4 pb-12">
      <RequestDetails request={request} />
    </main>
  );
};

export default RequestPage;
