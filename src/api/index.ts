"use server";
import { revalidatePath } from "next/cache";
import prisma from "@/lib/prisma";
import * as z from "zod";
import { cookies } from "next/headers";
import { formSchema } from "@/components/Requests/CreateRequestForm";
import { commentFormSchema } from "@/components/CommentInput";

// Gets the current user from cookies or chooses a random one if one does not exist
const getCurrentUser = () => {
  const cookieStore = cookies();
  const userId = cookieStore.get("userId");

  if (!userId?.value) {
    const randomUserId = Math.floor(Math.random() * 5);
    cookieStore.set("userId", "" + randomUserId);
    return randomUserId;
  }

  return parseInt(userId.value, 10);
};

// Manually sets the current user
export const setCurrentUser = (formData: FormData) => {
  const data: any = Array.from(formData.entries()).reduce(
    (accum, [key, value]) => {
      accum[key] = value;

      return accum;
    },
    {},
  );

  const cookieStore = cookies();
  cookieStore.set("userId", "" + data.id);

  return true;
};

export const getAllRequests = async () => {
  const allRequests = await prisma.request.findMany({
    include: {
      comments: true,
      createdBy: true,
    },
  });

  return allRequests;
};

export const getRequest = async ({ id }: { id: number }) => {
  const request = await prisma.request.findFirst({
    where: { id },
    include: {
      comments: {
        include: {
          user: true,
        },
      },
      createdBy: true,
    },
  });

  return request;
};

export const createRequest = async (formData: FormData) => {
  const data: z.infer<typeof formSchema> = Array.from(
    formData.entries(),
  ).reduce((accum, [key, value]) => {
    accum[key] = value;

    return accum;
  }, {});
  const userId = getCurrentUser();

  try {
    const newRequest = await prisma.request.create({
      data: {
        title: data.title,
        summary: data.summary,
        priority: data.priority,
        numVotes: 0,
        categories: [data.category],
        // @ts-ignore
        userId,
      },
    });

    revalidatePath("/requests");

    return {
      status: 200,
      data: newRequest,
    };
  } catch (error) {
    console.log("ERROR", error);
    return {
      status: 500,
      data: { message: "Something went wrong" },
    };
  }
};

export const createUser = async (formData: FormData) => {
  try {
    const newUser = await prisma.user.create({
      data: {
        firstName: "Scott",
        lastName: "Cazan",
        profilePic: "/profile3.png",
        anonymous: true,
      },
    });

    return {
      status: 200,
      data: newUser,
    };
  } catch (error) {
    return {
      status: 500,
      data: { message: "Something went wrong" },
    };
  }
};

export const createComment = async (formData: FormData) => {
  const data: z.infer<any> = Array.from(formData.entries()).reduce(
    (accum, [key, value]) => {
      accum[key] = value;

      return accum;
    },
    {},
  );

  const userId = getCurrentUser();

  try {
    const newComment = await prisma.comment.create({
      data: {
        userId,
        requestId: parseInt(data.requestId, 10),
        text: data.comment,
      },
    });

    revalidatePath(`/requests/`);
    revalidatePath(`/requests/${data.requestId}`);

    return {
      status: 200,
      data: newComment,
    };
  } catch (error) {
    console.log("ERROR", error);
    return {
      status: 500,
      data: { message: "Something went wrong" },
    };
  }
};

export const castVote = async (formData: FormData) => {
  const data: any = Array.from(formData.entries()).reduce(
    (accum, [key, value]) => {
      accum[key] = value;

      return accum;
    },
    {},
  );
  const userId = getCurrentUser();

  try {
    const newVote = await prisma.request.update({
      where: { id: parseInt(data.requestId, 10) },
      data: {
        numVotes: { increment: 1 },
        votes: {
          create: {
            userId,
          },
        },
      },
    });

    revalidatePath(`/requests/`);
    revalidatePath(`/requests/${data.requestId}`);

    return {
      status: 200,
      data: newVote,
    };
  } catch (error) {
    console.log("ERROR", error);
    return {
      status: 500,
      data: { message: "Something went wrong" },
    };
  }
};
