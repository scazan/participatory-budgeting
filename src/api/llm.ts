// @ts-nocheck
import OpenAI from "openai";
const configuration = {
  apiKey: process.env.OPENAI_API_KEY,
};

const openai = new OpenAI(configuration);

const ChatCompletionRequestMessageRoleEnum = {
  System: "system",
  User: "user",
  Assistant: "assistant",
  Function: "function",
};

const getPrompt = ({ query }) => {
  const systemIntro = `You are an assistant trained to create expret advice based on a given proposal.`;

  return {
    messages: [
      {
        role: ChatCompletionRequestMessageRoleEnum.System,
        content: systemIntro,
      },
      {
        role: ChatCompletionRequestMessageRoleEnum.User,
        content: `Proposal: ${query}

        Give a concise critique on the proposal above as if you were an expert on the matter. If you don't have a response simply state "I don't know" and nothing else. Your response shoule be less than 30 words long.
        `,
      },
    ],
  };
};

const createChatCompletion = async (input: any): Promise<any> => {
  const { model, messages, temperature } = input;

  const response = await openai.chat.completions.create({
    model,
    messages: messages.messages,
    temperature,
  });

  return response;
};

export const getExpertOpinion = async (proposalText: string) => {
  const messages = getPrompt({
    query: proposalText,
    previousQueries: new Set(),
  });

  const result = await createChatCompletion({
    model: "gpt-3.5-turbo",
    messages,
    temperature: 0.8,
  });

  return JSON.stringify(result.choices[0].message);
};
