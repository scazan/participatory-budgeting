export const UpArrow = () => (
  <svg
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    className="w-6"
  >
    <path
      d="M12 7.00001L12 17M12 7.00001L16 11M12 7.00001L8 11"
      stroke="#0396A6"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
