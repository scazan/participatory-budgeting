import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { cn } from "@/lib/utils";

export const SiteHeader = (props: { className?: string }) => {
  const { className } = props;
  return (
    <header className={cn(className, "flex gap-4 items-center ")}>
      <div className="min-w-[1.5rem] w-6 h-6">
        <svg
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className="w-full"
        >
          <g id="Menu">
            <path
              id="shape"
              d="M4 7H20M4 12H20M4 17H20"
              stroke="#142326"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </g>
        </svg>
      </div>

      <div className="w-full">
        <Select>
          <SelectTrigger className="w-[10.5rem]">
            <SelectValue placeholder="Pending Requests" />
          </SelectTrigger>
          <SelectContent>
            <SelectItem value="pending">Pending Requests</SelectItem>
            <SelectItem value="decided">Decided Requests</SelectItem>
          </SelectContent>
        </Select>
      </div>

      <div className="min-w-[1.5rem] w-6 h-6">
        <svg
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className="w-full"
        >
          <path
            d="M19 14C20.1046 14 21 13.1046 21 12C21 10.8954 20.1046 10 19 10M19 14C17.8954 14 17 13.1046 17 12C17 10.8954 17.8954 10 19 10M19 14V20M19 10V4M12 16C10.8954 16 10 16.8954 10 18C10 19.1046 10.8954 20 12 20C13.1046 20 14 19.1046 14 18C14 16.8954 13.1046 16 12 16ZM12 16V4M5 8C6.10457 8 7 7.10457 7 6C7 4.89543 6.10457 4 5 4C3.89543 4 3 4.89543 3 6C3 7.10457 3.89543 8 5 8ZM5 8V20"
            stroke="#142326"
            strokeWidth="1.5"
            strokeLinecap="round"
          />
        </svg>
      </div>

      <div className="min-w-[1.5rem] w-6 h-6">
        <svg
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className="w-full"
        >
          <path
            d="M19.4697 20.5303C19.7626 20.8232 20.2374 20.8232 20.5303 20.5303C20.8232 20.2374 20.8232 19.7626 20.5303 19.4697L19.4697 20.5303ZM17.25 10.5C17.25 14.2279 14.2279 17.25 10.5 17.25V18.75C15.0563 18.75 18.75 15.0563 18.75 10.5H17.25ZM10.5 17.25C6.77208 17.25 3.75 14.2279 3.75 10.5H2.25C2.25 15.0563 5.94365 18.75 10.5 18.75V17.25ZM3.75 10.5C3.75 6.77208 6.77208 3.75 10.5 3.75V2.25C5.94365 2.25 2.25 5.94365 2.25 10.5H3.75ZM10.5 3.75C14.2279 3.75 17.25 6.77208 17.25 10.5H18.75C18.75 5.94365 15.0563 2.25 10.5 2.25V3.75ZM20.5303 19.4697L16.3428 15.2821L15.2821 16.3428L19.4697 20.5303L20.5303 19.4697Z"
            fill="#142326"
          />
        </svg>
      </div>
    </header>
  );
};
