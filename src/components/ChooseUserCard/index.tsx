"use client";
import { useRouter } from "next/navigation";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { setCurrentUser } from "@/api";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import Link from "next/link";
import { Card, CardContent } from "@/components/ui/card";

export const ChooseUserCard = () => {
  const router = useRouter();
  const handleSubmit = (data) => {
    // construct a FormData object from the normalized object
    const formData = new FormData();

    formData.append("id", data.id);

    setCurrentUser(formData);

    router.push("/requests");
  };
  const form = useForm<z.infer<any>>({
    defaultValues: {
      id: 1,
    },
  });

  return (
    <Card className="w-full flex justify-center items-center h-[100vh]">
      <CardContent>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleSubmit)}
            className="space-y-8"
          >
            <FormField
              control={form.control}
              name="id"
              render={({ field }) => (
                <FormItem>
                  <FormDescription>
                    Choose a user or continue for a random user:
                  </FormDescription>
                  <Select
                    onValueChange={field.onChange}
                    defaultValue={field.value}
                  >
                    <FormControl>
                      <SelectTrigger>
                        <SelectValue placeholder="Select from the list" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      <SelectItem value="1">Beatrice</SelectItem>
                      <SelectItem value="2">Fernando</SelectItem>
                      <SelectItem value="4">Sukie</SelectItem>
                      <SelectItem value="3">Anonymous</SelectItem>
                    </SelectContent>
                  </Select>
                  <FormMessage />
                </FormItem>
              )}
            />

            <Button
              type="submit"
              className="w-full px-3 py-2 rounded-xl bg-tealLight text-teal text-sm leading-6 font-normal"
            >
              Confirm User
            </Button>
            <Button
              asChild
              className="w-full px-3 py-2 rounded-xl bg-tealLight text-teal text-sm leading-6 font-normal"
            >
              <Link href="/requests">Random</Link>
            </Button>
          </form>
        </Form>
      </CardContent>
    </Card>
  );
};
