"use client";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { CreateRequestForm } from "../CreateRequestForm";

export const CreateRequestButton = () => {
  return (
    <Popover>
      <PopoverTrigger className="fixed bottom-6 right-6 px-3 py-2 rounded-xl bg-tealLight text-teal text-sm leading-6 font-normal">
        Make a request
      </PopoverTrigger>
      <PopoverContent>
        <CreateRequestForm />
      </PopoverContent>
    </Popover>
  );
};
