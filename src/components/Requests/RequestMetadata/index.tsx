import { CardDescription } from "@/components/ui/card";

export const RequestMetadata = (props: {
  category: string;
  user: any;
  date: string;
}) => {
  // console.log("props", props);
  const {
    category,
    user = {
      firstName: "Beatrice",
      lastName: "Fitzgerald",
      anonymous: false,
      profilePic: "/profile1.png",
    },
    date,
  } = props;

  const days = new Date().getDate() - new Date(date).getDate();

  return (
    <div className="flex justify-between">
      <div className="flex items-center gap-2">
        {!user.anonymous && user.profilePic && (
          <div
            className="w-8 h-8"
            style={{
              background: `url('${user.profilePic}')`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              WebkitMaskImage: "url('/profile_mask.svg')",
              maskImage: "url('/profile_mask.svg')",
            }}
          ></div>
        )}
        <div>
          <div className="text-xs text-charcoal font-normal">
            {user.anonymous
              ? "Anonymous resident"
              : `${user.firstName} ${user.lastName.slice(0, 1)}.`}
          </div>
          <CardDescription>
            {days} days • {category}
          </CardDescription>
        </div>
      </div>
    </div>
  );
};
