"use client";

import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { createRequest } from "@/api";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Textarea } from "@/components/ui/textarea";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

export const formSchema = z.object({
  title: z.string().min(2, {
    message: "Title must be at least 2 characters.",
  }),
  summary: z.string().min(2, {
    message: "Summary must be at least 2 characters.",
  }),
  category: z.enum(["Transportation", "Leisure"]),
  // , {
  // message: "Please choose a category",
  // }),
  priority: z.enum(["low", "medium", "high"]),
  // , {
  // message: "Please choose a priority",
  // }),
});

const handleSubmit = (data, event) => {
  // construct a FormData object from the normalized object
  const formData = new FormData();
  // @ts-ignore
  Object.entries(data).forEach(([key, value]) => formData.append(key, value));

  createRequest(formData);
};

export const CreateRequestForm = () => {
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: "",
      summary: "",
      priority: "low",
    },
  });

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-8">
        <FormLabel>Create a new request</FormLabel>

        <FormField
          control={form.control}
          name="title"
          render={({ field }) => (
            <FormItem>
              <FormDescription>Title</FormDescription>
              <FormControl>
                <Input
                  placeholder="A concise one-sentence description"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="summary"
          render={({ field }) => (
            <FormItem>
              <FormDescription>Summary</FormDescription>
              <FormControl>
                <Textarea
                  placeholder="Give us details and a description of the request here"
                  className="resize-none h-32"
                  {...field}
                />
              </FormControl>
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="category"
          render={({ field }) => (
            <FormItem>
              <FormDescription>Category</FormDescription>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                  <SelectTrigger>
                    <SelectValue placeholder="Choose a category" />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  <SelectItem value="Transportation">Transportation</SelectItem>
                  <SelectItem value="Leisure">Leisure</SelectItem>
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="priority"
          render={({ field }) => (
            <FormItem>
              <FormDescription>Priority</FormDescription>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                  <SelectTrigger>
                    <SelectValue
                      className="bg-medium"
                      placeholder="Choose a priority for the request"
                    />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  <SelectItem value="low">Low</SelectItem>
                  <SelectItem className="bg-medium" value="medium">
                    Medium
                  </SelectItem>
                  <SelectItem className="bg-destructive" value="high">
                    High
                  </SelectItem>
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button
          type="submit"
          className="w-full px-3 py-2 rounded-xl bg-tealLight text-teal text-sm leading-6 font-normal"
        >
          Create
        </Button>
      </form>
    </Form>
  );
};
