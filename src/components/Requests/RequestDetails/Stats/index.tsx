export const Stats = (props: { numVotes?: number; numComments?: number }) => {
  const { numVotes = 0, numComments = 0 } = props;
  return (
    <div className="flex gap-4 w-full">
      <div>
        <span className="font-bold">{numVotes}</span> upvotes
      </div>
      <div>
        <span className="font-bold">{numComments}</span> comments
      </div>
    </div>
  );
};
