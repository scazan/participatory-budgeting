import { Card, CardContent, CardHeader } from "@/components/ui/card";
import { Stats } from "./Stats";
import { RequestMetadata } from "../RequestMetadata";
import { Title } from "../Title";
import Link from "next/link";
import { Actions } from "./Actions";
import { Section } from "./Section";
import { Comment } from "../Comment";
import { CommentInput } from "@/components/CommentInput";
import { SuggestComment } from "../SuggestComment";

export const RequestDetails = ({ request }) => {
  const {
    title,
    summary,
    priority,
    comments = [],
    numVotes,
    createdAt,
    createdBy,
    categories,
  } = request;

  return (
    <>
      <Card>
        <CardHeader className="px-4">
          <div className="flex w-full items-center ">
            <div className="flex w-full gap-2 items-center">
              <Link href={`/requests/`} className="w-6 h-6">
                <svg
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-6 h-6"
                >
                  <path
                    d="M4 12L20 12M4 12L10 6M4 12L10 18"
                    stroke="#142326"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </Link>
              <RequestMetadata
                category={categories[0]}
                date={createdAt}
                user={createdBy}
              />
            </div>
            <SuggestComment summary={summary} />
          </div>
          <Title title={title} priority={priority} />
        </CardHeader>
        <CardContent className="px-4">
          <p>{summary}</p>
        </CardContent>
        <Section className="border-b-0 border-t-[0.5px]">
          <Stats numComments={comments.length} numVotes={numVotes} />
        </Section>
      </Card>

      <Actions requestId={request.id} />

      {comments.map((comment) => (
        <Comment
          key={comment.id}
          user={comment.user}
          text={comment.text}
          date={createdAt}
        />
      ))}

      <Section className="border-b-0 border-t-[0.5px] fixed bottom-0 p-4">
        <CommentInput requestId={request.id} />
      </Section>
    </>
  );
};
