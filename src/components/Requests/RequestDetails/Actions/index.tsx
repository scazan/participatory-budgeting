"use client";
import { CommentBubble } from "@/components/img/CommentBubble";
import { ShareIcon } from "@/components/img/ShareIcon";
import { UpArrow } from "@/components/img/UpArrow";
import { Section } from "../Section";
import { castVote } from "@/api";

export const Actions = (props: {
  requestId: number;
  numVotes?: number;
  numComments?: number;
}) => {
  const { requestId } = props;

  const castVoteAction = () => {
    const formData = new FormData();
    formData.append("requestId", "" + requestId);

    castVote(formData);
  };

  return (
    <Section className="text-teal">
      <div className="flex gap-1" onClick={castVoteAction}>
        <UpArrow /> Upvote
      </div>
      <div className="flex gap-1">
        <CommentBubble /> Comment
      </div>

      <div className="flex gap-1">
        <ShareIcon /> Share
      </div>
    </Section>
  );
};
