import { cn } from "@/lib/utils";

export const Section = ({ children, className }) => {
  return (
    <div
      className={cn(
        "flex gap-4 w-full items-center justify-between py-2 px-4 border-b-[0.5px] border-veryLightGray",
        className,
      )}
    >
      {children}
    </div>
  );
};
