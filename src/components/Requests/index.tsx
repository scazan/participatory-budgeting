import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { List } from "./List";

export const Requests = (props: { requests: any }) => {
  const { requests } = props;

  return (
    <Tabs defaultValue="recent" className="w-full">
      <TabsList>
        <TabsTrigger value="recent">Recent</TabsTrigger>
        <TabsTrigger value="trending">Trending</TabsTrigger>
        <TabsTrigger value="editable">Editable</TabsTrigger>
        <TabsTrigger value="restricted">Restricted</TabsTrigger>
      </TabsList>
      <section>
        <TabsContent value="recent">
          <List requests={requests} />
        </TabsContent>
        <TabsContent value="trending">
          <List requests={requests} />
        </TabsContent>
        <TabsContent value="editable">
          <List requests={requests} />
        </TabsContent>
        <TabsContent value="restricted">
          <List requests={requests} />
        </TabsContent>
      </section>
    </Tabs>
  );
};
