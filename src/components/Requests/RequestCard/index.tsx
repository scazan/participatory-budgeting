import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
} from "@/components/ui/card";
import { Footer } from "./Footer";
import { RequestMetadata } from "../RequestMetadata";
import { Title } from "../Title";
import { SettingsDots } from "@/components/SettingsDots";

export const RequestCard = ({ request }) => {
  const {
    title,
    summary,
    priority,
    comments = [],
    numVotes,
    categories,
    createdAt,
    createdBy,
  } = request;

  const images = ["/soccer.png", "/street.png"];

  return (
    <Card>
      <CardHeader>
        <div className="flex justify-between">
          <RequestMetadata
            category={categories[0]}
            date={createdAt}
            user={createdBy}
          />
          <SettingsDots />
        </div>
        <Title title={title} priority={priority} />
      </CardHeader>
      <CardContent>
        <p className="max-h-16 overflow-hidden">{summary}</p>
        {Math.random() > 0.75 && (
          <div className="min-w-fit w-16 max-h-12 overflow-hidden">
            <img
              src={`${images[Math.round(Math.random() * (images.length - 1))]}`}
              className="w-16"
            />
          </div>
        )}
      </CardContent>
      <CardFooter>
        <Footer numComments={comments.length} numVotes={numVotes} />
      </CardFooter>
    </Card>
  );
};
