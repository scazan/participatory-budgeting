import { CommentBubble } from "@/components/img/CommentBubble";
import { UpArrow } from "@/components/img/UpArrow";

export const Footer = (props: { numVotes?: number; numComments?: number }) => {
  const { numVotes = 0, numComments = 0 } = props;
  return (
    <div className="flex w-full gap-4 text-teal text-sm font-normal">
      <div className="flex items-center gap-1">
        <UpArrow />
        {numVotes} upvotes
      </div>

      <div className="flex items-center gap-1">
        <CommentBubble />
        {numComments} comments
      </div>
    </div>
  );
};
