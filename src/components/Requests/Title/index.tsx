import { Badge } from "@/components/ui/badge";
import { CardTitle } from "@/components/ui/card";

export const Title = ({
  title,
  priority,
}: {
  title: string;
  priority: "low" | "medium" | "high";
}) => {
  return (
    <CardTitle className="flex gap-2 text-black text-md font-normal">
      {title} <Badge variant={priority}>{priority}</Badge>
    </CardTitle>
  );
};
