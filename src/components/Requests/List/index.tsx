import Link from "next/link";
import { RequestCard } from "../RequestCard";

export const List = (props: { requests: any }) => {
  const { requests } = props;
  return (
    <div>
      {requests.map((request) => (
        <Link href={`/requests/${request.id}`} key={request.id}>
          <RequestCard request={request} />
        </Link>
      ))}
    </div>
  );
};
