import { Card, CardContent, CardHeader } from "@/components/ui/card";
import { RequestMetadata } from "../RequestMetadata";
import { Section } from "../RequestDetails/Section";

export const Comment = (props: any) => {
  const { user, date, text } = props;

  return (
    <>
      <Card>
        <CardHeader>
          <div className="flex justify-between">
            <RequestMetadata
              category="Transportation Council Member"
              user={user}
              date={date}
            />
          </div>
        </CardHeader>
        <CardContent className="flex-col">
          <p className="max-h-16 overflow-hidden">{text}</p>
          <Section className="border-b-0 p-0 pt-2">
            <div className="text-teal flex gap-4 w-full">
              <div>4 Likes</div>
              <div>Reply</div>
            </div>
          </Section>
        </CardContent>
      </Card>
    </>
  );
};
