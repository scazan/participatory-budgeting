export const SettingsDots = () => {
  return (
    <div className="w-4 h-4">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16 16"
        fill="none"
        className="w-full"
      >
        <path
          d="M8 8.66666C8.36819 8.66666 8.66667 8.36818 8.66667 7.99999C8.66667 7.63181 8.36819 7.33333 8 7.33333C7.63181 7.33333 7.33333 7.63181 7.33333 7.99999C7.33333 8.36818 7.63181 8.66666 8 8.66666Z"
          stroke="#3A4649"
          strokeWidth="1.33333"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M12.6667 8.66666C13.0349 8.66666 13.3333 8.36818 13.3333 7.99999C13.3333 7.63181 13.0349 7.33333 12.6667 7.33333C12.2985 7.33333 12 7.63181 12 7.99999C12 8.36818 12.2985 8.66666 12.6667 8.66666Z"
          stroke="#3A4649"
          strokeWidth="1.33333"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M3.33333 8.66666C3.70152 8.66666 4 8.36818 4 7.99999C4 7.63181 3.70152 7.33333 3.33333 7.33333C2.96514 7.33333 2.66667 7.63181 2.66667 7.99999C2.66667 8.36818 2.96514 8.66666 3.33333 8.66666Z"
          stroke="#3A4649"
          strokeWidth="1.33333"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </div>
  );
};
