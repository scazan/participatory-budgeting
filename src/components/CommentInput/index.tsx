"use client";

import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { createComment, createRequest } from "@/api";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";

export const commentFormSchema = z.object({
  comment: z.string().min(2, {
    message: "Comment must be at least 2 characters.",
  }),
});

export const CommentInput = (props: { requestId: number }) => {
  const { requestId } = props;
  const handleSubmit = (data) => {
    // construct a FormData object from the normalized object
    const formData = new FormData();
    // @ts-ignore
    Object.entries(data).forEach(([key, value]) => formData.append(key, value));

    formData.append("requestId", "" + requestId);

    createComment(formData);
    form.reset();
  };
  const form = useForm<z.infer<typeof commentFormSchema>>({
    resolver: zodResolver(commentFormSchema),
    defaultValues: {
      comment: "",
    },
  });

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(handleSubmit)}
        className="space-y-8 w-full"
      >
        <FormField
          control={form.control}
          name="comment"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  placeholder="Write a comment"
                  className="text-darkGray font-normal h-10 p-3"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
      </form>
    </Form>
  );
};
